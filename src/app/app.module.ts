import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CreateRequestPage } from '../pages/create-request/create-request';
import { IssueListPage } from '../pages/issue-list/issue-list';
import { customHttpProvider } from '../providers/remote-service/remote-service';
import { UpdateRequestPage } from '../pages/update-request/update-request';
import { MenuPageModule } from '../pages/menu/menu.module';
import {IssueListPageModule} from '../pages/issue-list/issue-list.module'
import { CreateRequestPageModule } from '../pages/create-request/create-request.module';
import { ProfilePage } from '../pages/profile/profile';
import { EditIssuePage } from '../pages/edit-issue/edit-issue';
import {QueueListPage} from '../pages/queue-list/queue-list';
import {QueueListPageModule} from '../pages/queue-list/queue-list.module';
import {CreateQueuePage} from '../pages/create-queue/create-queue';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    QueueListPage,
    CreateQueuePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    MenuPageModule,
    IssueListPageModule,
    CreateRequestPageModule,
    QueueListPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    QueueListPage,
    CreateQueuePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    customHttpProvider
  ]
})
export class AppModule {}
