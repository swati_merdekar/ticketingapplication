import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IssueListPage } from './issue-list';

@NgModule({
  declarations: [
    IssueListPage,
  ],
  imports: [
    IonicPageModule.forChild(IssueListPage),
  ],
})
export class IssueListPageModule {}
