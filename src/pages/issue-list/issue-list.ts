import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController} from 'ionic-angular';
import { Http, Headers, Response, URLSearchParams , RequestOptions} from '@angular/http';
import { UpdateRequestPage } from '../update-request/update-request';
import 'rxjs/add/operator/map';
@IonicPage()
@Component({
  selector: 'page-issue-list',
  templateUrl: 'issue-list.html',
})
export class IssueListPage {
  issueList:any;
  items:any[]=[];
  options: RequestOptions;
  loading:any;
  tempIssueList:any;

  constructor(public nav: NavController, public navParams: NavParams,public http: Http,public loadingController:LoadingController){}

  ionViewDidLoad(){
    this.loading = this.loadingController.create({ 
        content: `Page Loading...`, }
      );
    this.loading.present();
        let headers: any = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.options = new RequestOptions({ headers: headers });
    var link = '/rt/api/v1.0/issues';
        this.http.post(link,'status:2',this.options)
    .map(res => res.json())
    .subscribe(data => {
        this.issueList = data.issue_list;
        this.tempIssueList = data.issue_list;
        this.sortId(true);
        this.initializeItems();
        this.loading.dismissAll();

    },  error => {
        this.loading.dismissAll();
        console.log("Oooops!"+error);
    });
  }
  
  initializeItems(){
        this.items = this.issueList;
  }
  viewDetail(detail){
  sessionStorage.setItem('clickedIssue',JSON.stringify(detail));
  this.nav.push('UpdateRequestPage',{id: detail}); 
  }



  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
          this.items = this.items.filter((item) => {
          return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
  }

sortId(descending:boolean):any[]{
  this.tempIssueList.sort(function(a,b){
    if(descending==true)
    return new Date(b.id).getTime()-new Date(a.id).getTime();
    if(descending==false)
    return new Date(a.id).getTime()-new Date(b.id).getTime();
  });
  console.log(this.tempIssueList);
  return this.tempIssueList;
}

/*sortDueDate(descending:boolean):any[]{
  this.tempIssueList.sort(function(a,b){
    if(descending==true)
    return new Date(b.created_on).getTime()-new Date(a.created_on).getTime();
    if(descending==false)
    return new Date(a.created_on).getTime()-new Date(b.created_on).getTime();
  });
  console.log(this.arr);
  return this.arr;
}

sortCreateDate(descending:boolean):any[]{
  this.arr.sort(function(a,b){
    if(descending==true)
    return new Date(b.created_on).getTime()-new Date(a.created_on).getTime();
    if(descending==false)
    return new Date(a.created_on).getTime()-new Date(b.created_on).getTime();
  });
  console.log(this.arr);
  return this.arr;
}

sortStatus(descending:boolean):any[]{
  this.arr.sort(function(a,b){
    if(descending==true)
    return b.status-a.status;
    if(descending==false)
    return a.status-b.status;
  });
  console.log(this.arr);
  return this.arr;
}*/

}