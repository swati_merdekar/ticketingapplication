import { Component } from '@angular/core';
import { Http, Headers, Response, URLSearchParams , RequestOptions} from '@angular/http';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';
import { CreateRequestPage } from '../create-request/create-request';
import { IssueListPage } from '../issue-list/issue-list';
import {CreateQueuePage} from '../create-queue/create-queue';

import 'rxjs/add/operator/map';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    authForm: FormGroup;
    errorMessage:String;
    options: RequestOptions;

    constructor(public nav: NavController, public navParams: NavParams, public formBuilder: FormBuilder,public http: Http) {
        this.nav = nav;
 //       this.nav.push(CreateQueuePage);
        //this.nav.setRoot('QueueMenuPage'); 
        this.authForm = formBuilder.group({
            username: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(30)])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(8)])]
        });
    }
 
    onSubmit(value: any): void { 
        if(this.authForm.valid) {
            let headers: any = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            this.options = new RequestOptions({ headers: headers });
            var link = '/login';
                var jsonObject =[];
                //var data = JSON.stringify();
                  this.http.post(link, 'email='+value.username+'&password='+value.password,this.options )
                 .map(res => res.json())
                 .subscribe(data => {
                  console.log(data);
                  sessionStorage.setItem('user',data);
                  this.nav.push('IssueListPage'); 
                  this.nav.setRoot('MenuPage'); 
                },error => {
                  this.errorMessage = "Invalid username or password";
                });
             }
    }
}
