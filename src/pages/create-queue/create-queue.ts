import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder,FormGroup, Validators,ValidatorFn,AbstractControl} from '@angular/forms'
import { Http, Headers, Response, URLSearchParams , RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the CreateQueuePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-queue',
  templateUrl: 'create-queue.html',
})
export class CreateQueuePage {
  createQueueForm:FormGroup;
  priority:any[]=[{text:"Low",value:'L'},{text:"Medium",value:'M'},{text:"High",value:'H'}];
  constructor(public http:Http,public formBuilder:FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
  this.generateForm();
    decodeURIComponent
}
onSubmit(createQueueForm:FormGroup){
console.log(createQueueForm);
this.genObject();
}
  generateForm(){
    this.createQueueForm=this.formBuilder.group({
      queueName:['',[Validators.required, Validators.minLength(8), Validators.maxLength(30)]],
      owner:['',Validators.required],
      group:['',Validators.required],
      closureTime:['',[Validators.required,Validators.min(1),Validators.max(99)]],
      escalationTime:['',[Validators.required,Validators.min(1),Validators.max(99)]],
      canAssignSelf:[false,Validators.required],
      workFlowOptions:['',Validators.required],
      mandatoryRootCause:[false,Validators.required]
    });
  }

  genObject(){
  var temp={"name":this.createQueueForm.value.queueName,"owner":2, "group":2 , "escalation_time_interval_hours":this.createQueueForm.value.escalationTime, "workflow_id":this.createQueueForm.value.workFlowOptions, "root_cause_mandatory": this.createQueueForm.value.mandatoryRootCause, "auto_start_wf": false, "auto_start_on_resolve": true, "closure_time_interval_hours": this.createQueueForm.value.closureTime};
    console.log(temp);
    let headers: any = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

  this.http.post('/admin/api/v1.0/queues',temp,options)
  .map(res=>res.json())
  .subscribe(data=>{
    console.log(data);
  },err=>{
    console.log(err);
  });
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateQueuePage');
  }

}
