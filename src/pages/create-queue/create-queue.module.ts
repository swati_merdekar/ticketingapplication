import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateQueuePage } from './create-queue';

@NgModule({
  declarations: [
    CreateQueuePage,
  ],
  imports: [
    IonicPageModule.forChild(CreateQueuePage),
  ],
})
export class CreateQueuePageModule {}
