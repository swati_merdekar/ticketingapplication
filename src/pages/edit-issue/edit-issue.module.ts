import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditIssuePage } from './edit-issue';

@NgModule({
  declarations: [
    EditIssuePage,
  ],
  imports: [
    IonicPageModule.forChild(EditIssuePage),
  ],
})
export class EditIssuePageModule {}
