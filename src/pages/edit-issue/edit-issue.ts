import { Component,ElementRef,Input,ViewChild} from '@angular/core';
import { Http, Headers, Response, URLSearchParams , RequestOptions} from '@angular/http';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { IssueListPage } from '../issue-list/issue-list';

@IonicPage()
@Component({
  selector: 'page-edit-issue',
  templateUrl: 'edit-issue.html',
})
export class EditIssuePage {
  @ViewChild('fileInput') files:ElementRef;
  authForm: FormGroup;
  options: RequestOptions;
  id: any;
  issue_status_list:any;
  issue_type_list:any;
  product_list:any;
  queue_list:any;
  user_list:any;
  type={
    1:'Install',
    2:'AMC',
    3:'Under Warranty',
    4:'Out of Warranty'
  }
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  priorityList: any = [{'text':'High','value':'H'}, {'text':'Medium','value':'M'}, {'text':'Low','value':'L'}];
  request: any= [{'text':'Install','value':'1'},{'text': 'AMC','value':'2'}, {'text':'Under warranty','value':'3'},{'text':' Out of warranty','value':'4'}];
  status: any = [{'text':'New','value':'1'},{'text': ' Awaiting assignment','value':'2'}, {'text':'Assigned','value':'3'},{'text':'Customer closed','value':'4'}];
  product: any = [{'text':'product 1','value':'1'},{'text': 'product 2','value':'2'}, {'text':'product 3','value':'3'},{'text':'product 4','value':'4'}];
  category: any = [{'text':'product 1','value':'1'},{'text': 'product 2','value':'2'}, {'text':'product 3','value':'3'},{'text':'product 4','value':'4'}];

    constructor(public nav: NavController, public navParams: NavParams, public formBuilder: FormBuilder,public http: Http) {
        this.id =JSON.parse(sessionStorage.getItem('clickedIssue'));
        this.nav = nav;
        this.authForm = formBuilder.group({
            title: [this.id.title,[Validators.required, Validators.minLength(8), Validators.maxLength(30)]],
            unit_sr_no: [this.id.xdata.unit_sr_no,[Validators.required, Validators.minLength(8)]],
            due_date: [new Date(this.id.due_date).toISOString().slice(0, -1),[Validators.required]],
            unit_location: [this.id.xdata.unit_location,[Validators.required,Validators.minLength(8), Validators.maxLength(30)]],
            pinCode: ['', [Validators.required, Validators.minLength(6),Validators.maxLength(6)]],
            email: [this.id.requestor,[Validators.required,Validators.pattern(this.emailPattern)]],
            contact: ['', [Validators.required,Validators.minLength(10),Validators.maxLength(10)]],
            priority: [this.id.priority, [Validators.required]],
            type: [this.id.type_id, [Validators.required]],
            status: [this.id.status, [Validators.required]],
            name: [this.id.assignee_name, [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
            description: [this.id.description, [Validators.required]],
            product:[this.id.product_id,[Validators.required]],
            category:[''],
            file:[''],
            queue:[this.id.q_id,[Validators.required]],
            requested_on: [''],
        });
    }
  ionViewDidEnter(){
            console.log('ionViewDidLoad CreateRequestPage');
            var link = '/rt/api/v1.0/all_masters_list';
            this.http.get(link)
            .map(res => res.json())
            .subscribe(data => {
            this.issue_status_list = data.issue_status_list;
            this.issue_type_list = data.issue_type_list;
            console.log(this.issue_type_list);
            this.product_list = data.product_list;
            this.queue_list = data.queue_list;
            this.user_list = data.user_list;
            console.log(this.issue_status_list);
        },  error => {
            console.log("Oooops!"+error);
        });
  }
  onSubmit(value: any): void {
    let headers: any = new Headers();
    headers.append('Content-Type', 'multipart/form-data');
    this.options = new RequestOptions({ headers: headers });
    let inputEl: HTMLInputElement = this.files.nativeElement;
    let fileCount: number = inputEl.files.length;
    let formData=new FormData();
    formData.append('title',value.title);
    formData.append('description',value.description);
    formData.append('requestor',value.email);
    formData.append('queue_id','1');
    formData.append('type_id',value.type);
    formData.append('status',value.status);
    formData.append('requestor_id','2');
    formData.append('assigned_by',value.name);
    formData.append('assignee_group','1');
    formData.append('cc_user_ids','shantala@gmail.in, abc@gmail.com');
    formData.append('root_cause_id','1');
    formData.append('priority',value.priority);
    formData.append('recurrence','O');
    formData.append('external_ids','shan3tala@gmail.com');
    formData.append('due_date',"2018-06-27T10:10");
    formData.append('unit_sr_no',value.unit_sr_no);
    formData.append('unit_location',value.unit_location);
    //formData.append('mfiles',value.file)
    console.log(formData);

    formData.append('product_id',value.product);
    console.log(formData);
    
    let files :FileList = inputEl.files;
    
    if(fileCount>0){
      for(var i=0;i<files.length;i++){
        formData.append('mfiles',files[i]);
      }
    }
    /*if(fileCount>0){
      for(var i=0;i<fileCount;i++){
        formData.append('mfiles',inputEl.files.item(i));
      }
    }*/
    console.log(fileCount);
        if(this.authForm.valid) {
			let headers: any = new Headers();
            var link = '/rt/api/v1.0/issues/'+this.id.id;
            console.log(link);
                //var data = JSON.stringify();
                this.http.put(link, formData)
                .subscribe(data => {
                 console.log(data);
                 this.nav.push('IssueListPage');

                }, error => {
                 console.log("Oooops!"+error);
                });
        }
   }
}
