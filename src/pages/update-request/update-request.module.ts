import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateRequestPage } from './update-request';

@NgModule({
  declarations: [
    UpdateRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateRequestPage),
  ],
})
export class UpdateRequestPageModule {}
