import { Component } from '@angular/core';
import { EditIssuePage } from '../edit-issue/edit-issue';
import { Http, Headers, Response, URLSearchParams , RequestOptions} from '@angular/http';
import { IssueListPage } from '../issue-list/issue-list';
import 'rxjs/add/operator/map';
import {Platform, IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-update-request',
  templateUrl: 'update-request.html',
})
export class UpdateRequestPage {
  id: any;
  isBrowser:boolean=false;
  fileList:any=[];
  model:any={};
  loading:any;
  loadingDoc:any;
  options:RequestOptions;
  fileDescription:any;
  formData:FormData;
  updates:any;
  addUpadates:any;
  linkedDocuments:any;
  
  priority={
    'M':'Medium',
    'H':'High',
    'L':'Low'
  };
  type={
    1:'Install',
    2:'AMC',
    3:'Under Warranty',
    4:'Out of Warranty'
  }
  constructor(public platform:Platform,public http: Http, public navCtrl: NavController, public navParams: NavParams,public loadingController:LoadingController) {
    this.id =JSON.parse(sessionStorage.getItem('clickedIssue'));
    this.formData=new FormData();
    if(this.platform.is('core'))
    this.isBrowser=true;
    this.id =JSON.parse(sessionStorage.getItem('clickedIssue'));
        console.log(this.id);//The id that you pa
  }

  
  addFile(){
    console.log('called addFile');
    var a = document.createElement("input");
    a.setAttribute('type','file');
    a.setAttribute('multiple','true');
    document.body.appendChild(a);
    var thisRef=this;
    a.onchange=function(){
     thisRef.fileList=[];
     thisRef.fileList=a.files;
     console.log(thisRef.fileList);
     if(thisRef.fileList.length>0){
      for(var i=0;i<thisRef.fileList.length;i++){
        thisRef.formData.append('file_name',thisRef.fileList[i]);
        console.log(thisRef.fileList);
      }
    }   
  };
    a.click();
  }

  submitFile(){
    var link = '/rt/api/v1.0/issues/'+this.id.id+'/documents';
        this.formData.append("description",this.fileDescription)
        this.http.post(link,this.formData)
        .map(res => res.json())
        .subscribe(data => {
        console.log(data);  
        this.fileList=[];
        this.formData=new FormData();
        this.loadingDocument();
    },  error => {
        console.log("Oooops!"+error);
    });
  }

  ionViewDidEnter() {
    this.loadingDocument();
    this.loadingUpdates();
  }

  edit(){
  	this.navCtrl.push('EditIssuePage');
  }

  addUpdates(){
        let headers: any = new Headers();
        headers.append('Content-Type', 'Application/json');
        this.options= new RequestOptions({ headers: headers });
        var link = '/rt/api/v1.0/issues/'+this.id.id+'/updates';
        this.http.post(link,{"remarks":this.addUpadates},this.options)
        .map(res => res.json())
        .subscribe(data => {
        console.log(data);  
        this.loadingUpdates();
    },error => {
        console.log("Oooops!"+error);
    });
  }

  loadingDocument(){
        this.loading = this.loadingController.create({ 
        content: `Page Loading...`, }
        );
        this.loading.present();
        console.log('ionViewDidLoad UpdateRequestPage');
        var link = '/rt/api/v1.0/issues/'+this.id.id+'/documents';
        this.http.get(link)
        .map(res => res.json())
        .subscribe(data => {
        console.log(data);  
        this.linkedDocuments = data.rt_doc_list;
        this.loading.dismiss();
    },  error => {
        this.loading.dismiss();
        console.log("Oooops!"+error);
    });
  }

  loadingUpdates(){
        this.loadingDoc = this.loadingController.create({ 
        content: `Page Loading...`, }
        );
        this.loadingDoc.present();
        console.log('ionViewDidLoad UpdateRequestPage');
        var link = '/rt/api/v1.0/issues/'+this.id.id+'/updates';
        this.http.get(link)
        .map(res => res.json())
        .subscribe(data => {
        console.log(data);  
        this.updates = data.updates;
        this.loadingDoc.dismiss();
    },  error => {
        this.loadingDoc.dismiss();
        console.log("Oooops!"+error);
    });
  }

  downloadFile(res){
        this.loading = this.loadingController.create({ 
        content: `Page Loading...`, }
        );
        this.loading.present();
        console.log('ionViewDidLoad UpdateRequestPage');
        let headers: any = new Headers();
        headers.append('Content-Type', 'application/json');
        this.options = new RequestOptions({ headers: headers });
        var link = '/archival/api/v1.0/archival_docs/'+res.id
        this.http.get(link,this.options)
        .subscribe(data => {
        console.log(data);
        this.downloadFileId(data.text(),data.headers.get('Content-Type'),res.file_name);  
        this.loading.dismissAll();
    },  error => {
        this.loading.dismissAll();
        console.log("Oooops!"+error);
    });
  }

downloadFileId(data: string , fileType:string , fileName:string){
  /* var blob = new Blob([data], { type: 'text/csv' });
   var url= window.URL.createObjectURL(blob);
   window.open(url,'report.txt');
 */
       var a = document.createElement("a");
       document.body.appendChild(a);
       var blob = new Blob([data],{type:fileType});
       console.log(blob);
       var url= window.URL.createObjectURL(blob);
       a.href=url;
       a.download=fileName;
       a.click();
       window.URL.revokeObjectURL(url);
 }

}